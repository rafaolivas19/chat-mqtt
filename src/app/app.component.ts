import { Component, OnDestroy } from '@angular/core';
import { MqttService, IMqttMessage } from 'ngx-mqtt';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
    title = 'angular-mqtt-demo';
    message: string;
    topic: string;
    subscription: string;
    subscriptions: SubscriptionItem[] = [];
    messages: string[] = [];

    constructor(private mqtt: MqttService) { }

    ngOnDestroy(): void {
        for (const subscription of this.subscriptions) {
            subscription.obj.unsubscribe();
        }
    }

    addSubscription(event = null): void {
        if (!event || event && event.ctrlKey && event.keyCode === 13) {
            const match = this.subscriptions.find(x => x.name === this.subscription);

            if (!match) {
                const subscription = this.mqtt.observe(this.subscription).subscribe((message: IMqttMessage) => {
                    this.messages.push(`${message.topic}: ${message.payload.toString()}`);
                });

                this.subscriptions.push({ obj: subscription, name: this.subscription });
            }

            this.subscription = '';
        }
    }

    removeSubscription(subscription: SubscriptionItem): void {
        const match = this.subscriptions.find(x => x.name === subscription.name);
        match.obj.unsubscribe();
        const index = this.subscriptions.indexOf(match);
        this.subscriptions.splice(index, 1);
    }

    sendMessage(event: any = null): void {
        if (!event || event && event.ctrlKey && event.keyCode === 13) {
            if (!this.topic) {
                alert('Debes indicar un tema primero');
            }
            else if (!this.message) {
                alert('No indicaste un mensaje');
            }
            else {
                this.mqtt.unsafePublish(this.topic, this.message);
                this.messages.push(`Tú (${this.topic}): ${this.message}`);
                this.message = '';
            }
        }
    }
}

type SubscriptionItem = { obj: Subscription, name: string };
