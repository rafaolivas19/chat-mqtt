import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IMqttServiceOptions, MqttModule } from 'ngx-mqtt';

const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
    hostname: 'api.xenterglobal.com',
    port: 2096,
    path: '/mqtt',
    protocol: 'wss' // wss solo cuando hay un certificado SSL de por medio, de otro modo debe ser ws
};

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        MqttModule.forRoot(MQTT_SERVICE_OPTIONS)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
